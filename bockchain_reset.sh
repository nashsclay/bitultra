cd ~/.bitultra
sudo rm bitultrad.pid blk0001.dat db.log debug.log mncache.dat smsg.ini
sudo rm -R backups/
sudo rm -R database/
sudo rm -R smsgDB/
sudo rm -R smsgStore/
sudo rm -R txleveldb/
bitultrad stop
echo "Masternode restarted"